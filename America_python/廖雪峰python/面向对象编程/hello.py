#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2019/8/12 16:47
# @Author : chenxin
# @Site : 
# @File : hello.py
# @Software: PyCharm
class Hello(object):
    def hello(self,name='Alex'):
        print('Hello,%s.' % name)
